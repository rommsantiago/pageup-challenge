﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ParcelLibrary;

namespace Rules
{
    public class Rules
    {
        public Parcel ExecuteRule(Parcel parcel)
        {
            int getVolume = parcel.Width * parcel.Height * parcel.Depth;
            int getWeight = parcel.Weight;

            if (getWeight > 10)
            {
                if (RejectParcelRule(getWeight))
                    parcel.CostMultiplier = 0;
                else
                    parcel.CostMultiplier = HeavyParcelRule(getWeight);

                return parcel;
            }

            parcel.CostMultiplier = SmallParcelRule(getVolume) + MediumParcelRule(getVolume) + LargeParcelRule(getVolume);
            return parcel;

        }


        #region rules 
        private bool RejectParcelRule(int Weight)
        {
            return Weight > 50 ? true : false;
        }

        private int HeavyParcelRule(int Weight)
        {
            return Weight > 10 ? 15 : 0;
        }

        private double SmallParcelRule(int volume)
        {
            return volume < 1500 ? 0.05d : 0d;
        }

        private double MediumParcelRule(int volume)
        {
            return (volume >= 1500 && volume < 2500) ? 0.04d : 0d;

        }
        private double LargeParcelRule(int volume)
        {
            return volume >= 2500 ? 0.03d : 0d;

        }
        #endregion 
    }
}
