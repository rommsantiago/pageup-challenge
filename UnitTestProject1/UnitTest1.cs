using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using ParcelLibrary;
using Rules;

namespace ParcelTest
{
    [TestClass]
    public class UnitTest1
    {
        #region TestCategory

        [TestMethod]
        public void Test_HeavyParcel_Category()
        {
            Parcel heavyParcel = new Parcel();
            Rules.Rules rules = new Rules.Rules();

            heavyParcel.Weight = 16;
            heavyParcel.Height = 20;
            heavyParcel.Width = 5;
            heavyParcel.Depth = 20;

            heavyParcel = rules.ExecuteRule(heavyParcel);

            Assert.AreEqual(heavyParcel.Category, ParcelType.Heavy);

        }

        [TestMethod]
        public void Test_RejectParcel_Category()
        {
            Parcel rejectParcel = new Parcel();
            Rules.Rules rules = new Rules.Rules();

            rejectParcel.Weight = 116;
            rejectParcel.Height = 20;
            rejectParcel.Width = 5;
            rejectParcel.Depth = 20;
            rejectParcel = rules.ExecuteRule(rejectParcel);
            Assert.AreEqual(rejectParcel.Category, ParcelType.Rejected);

        }
        [TestMethod]
        public void Test_SmallParcel_Category()
        {
            Parcel smallParcel = new Parcel();
            Rules.Rules rules = new Rules.Rules();



            smallParcel.Weight = 10;
            smallParcel.Height = 1;
            smallParcel.Width = 1;
            smallParcel.Depth = 10;
            smallParcel = rules.ExecuteRule(smallParcel);
            Assert.AreEqual(smallParcel.Category, ParcelType.Small);

        }
        [TestMethod]
        public void Test_MediumParcel_Category()
        {
            Parcel mediumParcel = new Parcel();
            Rules.Rules rules = new Rules.Rules();

            mediumParcel.Weight = 10;
            mediumParcel.Height = 25;
            mediumParcel.Width = 25;
            mediumParcel.Depth = 3;
            mediumParcel = rules.ExecuteRule(mediumParcel);
            Assert.AreEqual(mediumParcel.Category, ParcelType.Medium);

        }
        [TestMethod]
        public void Test_LargeParcel_Category()
        {
            Parcel largeParcel = new Parcel();
            Rules.Rules rules = new Rules.Rules();

            largeParcel.Weight = 10;
            largeParcel.Height = 25;
            largeParcel.Width = 25;
            largeParcel.Depth = 25;
            largeParcel = rules.ExecuteRule(largeParcel);
            Assert.AreEqual(largeParcel.Category, ParcelType.Large);

        }

        #endregion

        #region TestAmountValue

        [TestMethod]
        public void Test_RejectParcel_Amount()
        {
            Parcel parcel = new Parcel();
            Rules.Rules rules = new Rules.Rules();

            parcel.Weight = 900;
            parcel.Height = 20;
            parcel.Width = 5;
            parcel.Depth = 20;

            parcel = rules.ExecuteRule(parcel);

            Assert.AreEqual(parcel.Cost, 0);
        }

        [TestMethod]
        public void Test_HeavyParcel_Amount()
        {
            Parcel parcel = new Parcel();
            Rules.Rules rules = new Rules.Rules();

            parcel.Weight = 10;
            parcel.Height = 20;
            parcel.Width = 5;
            parcel.Depth = 20;

            parcel = rules.ExecuteRule(parcel);

            Assert.AreEqual(parcel.Cost, 80d);
        }

        [TestMethod]
        public void Test_SmallParcel_Amount()
        {
            Parcel parcel = new Parcel();
            Rules.Rules rules = new Rules.Rules();

            parcel.Weight = 2;
            parcel.Height = 3;
            parcel.Width = 10;
            parcel.Depth = 12;

            parcel = rules.ExecuteRule(parcel);

            Assert.AreEqual(parcel.Cost, 18d);
        }

        [TestMethod]
        public void Test_MediumParcel_Amount()
        {
            Parcel parcel = new Parcel();
            Rules.Rules rules = new Rules.Rules();

            parcel.Weight = 10;
            parcel.Height = 25;
            parcel.Width = 25;
            parcel.Depth = 3;

            parcel = rules.ExecuteRule(parcel);

            Assert.AreEqual(parcel.Cost, 75d);
        }
        [TestMethod]
        public void Test_LargeParcel_Amount()
        {
            Parcel parcel = new Parcel();
            Rules.Rules rules = new Rules.Rules();

            parcel.Weight = 10;
            parcel.Height = 25;
            parcel.Width = 25;
            parcel.Depth = 23;

            parcel = rules.ExecuteRule(parcel);

            Assert.AreEqual(parcel.Cost, 431.25d);
        }





        #endregion



    }
}
