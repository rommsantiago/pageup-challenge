﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParcelLibrary
{
    public interface IParcel
    {
        int Weight { get; set; }
        int Height { get; set; }
        int Width { get; set; }
        int Depth { get; set; }

        ParcelType? Category { get; }

        double Cost { get; }
        double? CostMultiplier { get; set; }

    }


    public enum ParcelType
    {
        Rejected = 0,
        Heavy = 1,
        Small = 2,
        Medium = 3,
        Large = 4

    }
}
