﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParcelLibrary
{
    public class Parcel : IParcel
    {
        public int Weight { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
        public int Depth { get; set; }
        public ParcelType? Category
        {
            get
            {
                if (CostMultiplier == 0) return ParcelType.Rejected;
                if (CostMultiplier == 15d) return ParcelType.Heavy;
                if (CostMultiplier == 0.05d) return ParcelType.Small;
                if (CostMultiplier == 0.04d) return ParcelType.Medium;
                if (CostMultiplier == 0.03d) return ParcelType.Large;
                return null;
            }
        }
        public double Cost
        {
            get
            {
                if (this.Category == ParcelType.Heavy || this.Category == ParcelType.Rejected)
                    return this.Weight * (double)CostMultiplier;

                return (Height * Width * Depth) * (double)CostMultiplier; ;


            }
        }

        public double? CostMultiplier
        { get; set; }
    }
}
