﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ParcelLibrary;

namespace MainApplication
{
    class Program
    {
        static void Main(string[] args)
        {


            var ParcelCM = new Parcel();
            Rules.Rules rule = new Rules.Rules();

            ParcelCM.Weight = GetIntValue("Enter Weight kg : ");
            ParcelCM.Height = GetIntValue("Enter Height in cm : ");
            ParcelCM.Width = GetIntValue("Enter Width in cm : ");
            ParcelCM.Depth = GetIntValue("Enter Depth in cm : ");

            ParcelCM = rule.ExecuteRule(ParcelCM);


            //ParcelCM.Category = ParcelType.Heavy;
            Console.WriteLine(string.Format("Category : {0}", ParcelCM.Category));
            if (ParcelCM.Category == ParcelType.Rejected)
                Console.WriteLine(string.Format("Cost : N/A"));
            else
                Console.WriteLine(string.Format("Cost : ${0}", ParcelCM.Cost));

            Console.ReadLine();
        }



        private static int GetIntValue(string StringValue)
        {
            int number;
            bool check;

            do
            {
                Console.Write(StringValue);
                check = int.TryParse(Console.ReadLine(), out number);
                if (!check)
                    Console.WriteLine("Invalid input please try again...\n");
            }
            while (!check);

            return number;
        }
    }
}
